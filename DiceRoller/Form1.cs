﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiceRoller
{
    public partial class diceGameForm : Form
    {
        #region Declaration

        private Image[] diceImages;
        private int[] dice;
        private int[] diceResults;
        private Random rand;

        #endregion

        #region Initialization

        public diceGameForm()
        {
            InitializeComponent();
        }

        private void diceGameForm_Load(object sender, EventArgs e)
        {
            diceImages = new Image[7];
            diceImages[0] = Properties.Resources.dice_blank;
            diceImages[1] = Properties.Resources.dice_1;
            diceImages[2] = Properties.Resources.dice_2;
            diceImages[3] = Properties.Resources.dice_3;
            diceImages[4] = Properties.Resources.dice_4;
            diceImages[5] = Properties.Resources.dice_5;
            diceImages[6] = Properties.Resources.dice_6;

            dice = new int[5] {0, 0, 0, 0, 0};

            diceResults = new int[6] {0, 0, 0, 0, 0, 0};

            rand = new Random();
        }

        #endregion

        #region Private Methods

        private void btnRollDice_Click(object sender, EventArgs e)
        {
            RollDice();

            GetResults();

            ResetDice();
        }

        private void RollDice()
        {
            for (int i = 0; i < dice.Length; ++i)
            {
                dice[i] = rand.Next(1, 6 + 1);

                //Equivalent to the unfinished code below but way more concise
                diceResults[dice[i]]++;

                //switch (dice[i])
                //{
                //    case 1:
                //        diceResults[0]++;
                //        break;
                //    case 2:
                //        diceResults[0]++;
                //        break;
                //    case 1:
                //        diceResults[0]++;
                //        break;
                //    case 1:
                //        diceResults[0]++;
                //        break;
                //    case 1:
                //        diceResults[0]++;
                //        break;
                //    case 1:
                //        diceResults[0]++;
                //        break;

                //}
            }

            lblDie0.Image = diceImages[dice[0]];
            lblDie1.Image = diceImages[dice[1]];
            lblDie2.Image = diceImages[dice[2]];
            lblDie3.Image = diceImages[dice[3]];
            lblDie4.Image = diceImages[dice[4]];
        }

        private void GetResults()
        {
            bool fiveKind = false,
                fourKind = false,
                highStraight = false,
                lowStraight = false,
                fullHouse = false,
                threeKind = false,
                twoPair = false,
                onePair = false,
                haveSix = false,
                haveFive = false,
                haveFour = false,
                haveThree = false,
                haveTwo = false,
                haveOne = false;

            for (int i = 0; i < diceResults.Length; ++i)
            {
                if (diceResults[i] == 5)
                    fiveKind = true;
                else if (diceResults[i] == 4)
                    fourKind = true;
                else if (diceResults[1] == 1 &&
                         diceResults[2] == 1 &&
                         diceResults[3] == 1 &&
                         diceResults[4] == 1 &&
                         diceResults[5] == 1)
                    highStraight = true;
                else if (diceResults[0] == 1 &&
                         diceResults[1] == 1 &&
                         diceResults[2] == 1 &&
                         diceResults[3] == 1 &&
                         diceResults[4] == 1)
                    lowStraight = true;
                else if (diceResults[i] == 3)
                {
                    threeKind = true;

                    for (int j = 0; j < diceResults.Length; ++j)
                    {
                        if (diceResults[j] == 2)
                            fullHouse = true;
                    }
                }
                else if (diceResults[i] == 2)
                {
                    onePair = true;

                    for (int j = i + 1; j < diceResults.Length; ++j)
                    {
                        if (diceResults[j] == 2)
                            twoPair = true;
                    }
                }
            }

            for (int i = 0; i < dice.Length; ++i)
            {
                switch (dice[i])
                {
                    case 6:
                        haveSix = true;
                        break;
                    case 5:
                        haveFive = true;
                        break;
                    case 4:
                        haveFour = true;
                        break;
                    case 3:
                        haveThree = true;
                        break;
                    case 2:
                        haveTwo = true;
                        break;
                    case 1:
                        haveOne = true;
                        break;
                }
            }
        }

        private void ResetDice()
        {
            
        }
        #endregion
    }
}
