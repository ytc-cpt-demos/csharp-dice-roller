﻿namespace DiceRoller
{
    partial class diceGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDie0 = new System.Windows.Forms.Label();
            this.lblDie1 = new System.Windows.Forms.Label();
            this.lblDie2 = new System.Windows.Forms.Label();
            this.lblDie3 = new System.Windows.Forms.Label();
            this.lblDie4 = new System.Windows.Forms.Label();
            this.btnRollDice = new System.Windows.Forms.Button();
            this.lblResults = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDie0
            // 
            this.lblDie0.Image = global::DiceRoller.Properties.Resources.dice_blank;
            this.lblDie0.Location = new System.Drawing.Point(241, 74);
            this.lblDie0.Name = "lblDie0";
            this.lblDie0.Size = new System.Drawing.Size(50, 50);
            this.lblDie0.TabIndex = 0;
            // 
            // lblDie1
            // 
            this.lblDie1.Image = global::DiceRoller.Properties.Resources.dice_blank;
            this.lblDie1.Location = new System.Drawing.Point(297, 74);
            this.lblDie1.Name = "lblDie1";
            this.lblDie1.Size = new System.Drawing.Size(50, 50);
            this.lblDie1.TabIndex = 1;
            // 
            // lblDie2
            // 
            this.lblDie2.Image = global::DiceRoller.Properties.Resources.dice_blank;
            this.lblDie2.Location = new System.Drawing.Point(353, 74);
            this.lblDie2.Name = "lblDie2";
            this.lblDie2.Size = new System.Drawing.Size(50, 50);
            this.lblDie2.TabIndex = 2;
            // 
            // lblDie3
            // 
            this.lblDie3.Image = global::DiceRoller.Properties.Resources.dice_blank;
            this.lblDie3.Location = new System.Drawing.Point(409, 74);
            this.lblDie3.Name = "lblDie3";
            this.lblDie3.Size = new System.Drawing.Size(50, 50);
            this.lblDie3.TabIndex = 3;
            // 
            // lblDie4
            // 
            this.lblDie4.Image = global::DiceRoller.Properties.Resources.dice_blank;
            this.lblDie4.Location = new System.Drawing.Point(465, 74);
            this.lblDie4.Name = "lblDie4";
            this.lblDie4.Size = new System.Drawing.Size(50, 50);
            this.lblDie4.TabIndex = 4;
            // 
            // btnRollDice
            // 
            this.btnRollDice.Location = new System.Drawing.Point(282, 146);
            this.btnRollDice.Name = "btnRollDice";
            this.btnRollDice.Size = new System.Drawing.Size(200, 50);
            this.btnRollDice.TabIndex = 5;
            this.btnRollDice.Text = "Roll Dice";
            this.btnRollDice.UseVisualStyleBackColor = true;
            this.btnRollDice.Click += new System.EventHandler(this.btnRollDice_Click);
            // 
            // lblResults
            // 
            this.lblResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResults.Location = new System.Drawing.Point(244, 235);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(250, 50);
            this.lblResults.TabIndex = 6;
            this.lblResults.Text = "Roll the Dice";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diceGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lblResults);
            this.Controls.Add(this.btnRollDice);
            this.Controls.Add(this.lblDie4);
            this.Controls.Add(this.lblDie3);
            this.Controls.Add(this.lblDie2);
            this.Controls.Add(this.lblDie1);
            this.Controls.Add(this.lblDie0);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "diceGameForm";
            this.Text = "Dice Game";
            this.Load += new System.EventHandler(this.diceGameForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDie0;
        private System.Windows.Forms.Label lblDie1;
        private System.Windows.Forms.Label lblDie2;
        private System.Windows.Forms.Label lblDie3;
        private System.Windows.Forms.Label lblDie4;
        private System.Windows.Forms.Button btnRollDice;
        private System.Windows.Forms.Label lblResults;
    }
}

